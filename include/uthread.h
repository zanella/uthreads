#include <stdio.h>
#include <stdlib.h>
#include "../include/mutex.h"

int libuthread_init();
int uthread_create(void*(*start_routine)(void*), void* arg);
int uthread_yield();
int uthread_join(int thr);
void uthread_mutex_init (umutex_t *lock); 
int uthread_lock(umutex_t* lock);
int uthread_unlock(umutex_t* lock);
