#include <ucontext.h>

ucontext_t* make_context(void (*start_routine) (void), ucontext_t* endlink);
ucontext_t* make_context_args(void* (*start_routine) (void*), ucontext_t* endlink, void* arg);
