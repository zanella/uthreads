#include <stdlib.h>
#include <ucontext.h>

#define READY 1;
#define RUNNING 2;
#define BLOCKED 3;
typedef struct list_tcb LIST;
typedef struct thread_control_block{
	int thread_id;
	int state;
	ucontext_t *context;
	LIST* waiting;
}TCB;

typedef struct list_tcb{
	TCB *node;
	struct list_tcb *next;
}LIST;

typedef struct fifo_tcb{
	LIST* head;
	LIST* tail;
}FIFO;

FIFO* newFIFO();
TCB* newTCB(ucontext_t* thr_context, int tid);
LIST* newLIST();
int addTCBinFIFO(TCB* atcb, FIFO* afifo);
TCB* getFirst(FIFO* afifo);
void remove_thread(TCB* thread, FIFO* afifo);
void toRunning(TCB* atcb);
void toReady(TCB* atcb);
void toBlocked(TCB* atcb);
void addinWaitList(TCB* TCBblocking,TCB* TCBblocked);
