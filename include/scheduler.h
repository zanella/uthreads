#include <stdlib.h>
#include "thread_context.h"

#define MAXTHREADS 129
int initScheduler();
int addthread(ucontext_t* thr_context);
int inc_counters();
void endThread();
int next_thread();
ucontext_t* getEndContext();
void inc_pointers();
int yield_scheduler();
void printfifo();
void* getTCB(int thr);
void* getRunning();
int blocking(void* tcb_blocking, void* tcb_current);
int unbloking(void* atcb_v);
void toBlockedList(void* tcb);
void toReadyList(void* afifo);
