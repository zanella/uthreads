#include <stdio.h>
#include <stdlib.h>
#include "../include/uthread.h"
#define LINHA 3
#define COLUNA 3
/*
 * SOMA MATRIZ
 * Soma elementos de uma matriz
*/
int aux[LINHA] = {0};
int resultado = 0;
umutex_t mutex;
void *somaLinha(int a[COLUNA]) {
	int i;
	int k=0;    
	for(i=0;i<COLUNA;i++){
		k+=a[i];
		resultado+=a[i];
	}
	uthread_lock(&mutex);
		uthread_yield();
		i=0;
		while(aux[i]!=0){
			i++;
			if(i>=LINHA){
				break;
			}
		}
		aux[i] = k;
	uthread_unlock(&mutex);

}

int main(int argc, char *argv[]){
    int j,i;
    int m1[LINHA][COLUNA]={0};	
    int id0;
    
    for(i=0;i<LINHA;i++){
	for(j=0;j<COLUNA;j++){
		m1[i][j]=1;
    	}
    }
    libuthread_init();
    uthread_mutex_init(&mutex);
    for(i=0;i<LINHA;i++){
	id0 = uthread_create(somaLinha, m1[i]);
    }
	uthread_join(id0);
    for (i=0;i<LINHA;i++){
         printf("Soma da Linha %d : %d \n", i, aux[i]);
     }
	printf("Soma total: %d \n", resultado);
    return 0;
}
