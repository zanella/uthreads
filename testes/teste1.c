#include <stdio.h>
#include <stdlib.h>
#include "../include/uthread.h"
/*
 * TESTE 1
 * Testando limites na criação de threads
*/

void *funcA(int a) {
    printf(" %d -", a);
}

int main(int argc, char *argv[]){
    int j; 
    int id0, id1, id2;
    libuthread_init();
    printf("\n Criando 128 threads... \n");
    for(j=0; j<128; j++){
    	id0 = uthread_create(funcA, j);
	if(id0!=-1){
		id2=id0;
	}
    }
   uthread_join(id2);
   printf("\n Tentando criar 130 threads... \n");
    for(j=0; j<131; j++){
    	id0 = uthread_create(funcA, j);
	if(id0!=-1){
		id1=id0;
	}
    }
    uthread_join(id1);
    printf("\n\n");
    return 0;
}
