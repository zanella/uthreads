#include <stdio.h>
#include <stdlib.h>
#include "../include/uthread.h"
/*
 * TESTE 3
 * Usando o mutex e criando thread dentro de uma zona crítica
*/
umutex_t mutex;

void *fun1(int a){
	uthread_lock(&mutex);
		
		printf("B");
		uthread_yield();
		printf("C");
	uthread_unlock(&mutex);
}

void *fun2(int a) {
	int id0;
	int j;
	uthread_lock(&mutex);
		printf("A");		
		id0 = uthread_create(fun1, &j);
		uthread_yield();
		printf("D");
	uthread_unlock(&mutex);
	uthread_join(id0);
}
int main(int argc, char *argv[]){
    int j, id0, id1;
    libuthread_init();
    uthread_mutex_init(&mutex);
    id0 = uthread_create(fun1, &j);
    id1 = uthread_create(fun2, &j);
    uthread_join(id0);
    uthread_join(id1);
    printf("\n");
    return 0;
}
