#include <stdio.h>
#include <stdlib.h>
#include "../include/uthread.h"
/*
 * TESTE 1
 * Testando threads criando outras threads
*/

void *funcN(int a) {
    int id0;
    if(a>0){
    id0 = uthread_create(funcN, a-1);
    uthread_join(id0);
    }	
    printf(" %d ", a);
}

int main(int argc, char *argv[]){
    int j; 
    int id0;
    j=10;
    libuthread_init();
    id0 = uthread_create(funcN, j);
    uthread_join(id0);
    return 0;
}
