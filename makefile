all: libuthread.a
libuthread.a: uthread.o scheduler.o dispatcher.o thread_context.o thread_control_block.o
	ar crs lib/libuthread.a bin/uthread.o bin/scheduler.o bin/dispatcher.o bin/thread_context.o bin/thread_control_block.o
uthread.o: scr/uthread.c include/uthread.h include/mutex.h include/thread_context.h include/scheduler.h
	gcc -c scr/uthread.c -Wall
	mv uthread.o bin/uthread.o
scheduler.o: scr/scheduler.c include/scheduler.h include/dispatcher.h include/thread_context.h
	gcc -c scr/scheduler.c -Wall
	mv scheduler.o bin/scheduler.o
dispatcher.o: scr/dispatcher.c include/dispatcher.h include/thread_control_block.h
	gcc -c scr/dispatcher.c -Wall
	mv dispatcher.o bin/dispatcher.o
thread_context.o: scr/thread_context.c include/thread_context.h
	gcc -c scr/thread_context.c -Wall
	mv thread_context.o bin/thread_context.o
thread_control_block.o: scr/thread_control_block.c include/thread_control_block.h
	gcc -c scr/thread_control_block.c -Wall
	mv thread_control_block.o bin/thread_control_block.o
Clean:
	rm bin/*.o
	rm lib/*.a
