#include <stdio.h>
#include "../include/thread_control_block.h"

FIFO* newFIFO(){
	FIFO* tmp = (FIFO*)malloc(sizeof(FIFO));
	tmp->head = NULL;
	tmp->tail = NULL;
	return tmp;
}

TCB* newTCB(ucontext_t* thr_context, int tid){
	TCB* tmp = (TCB*)malloc(sizeof(TCB));
	tmp->thread_id = tid;
	tmp->state = READY;
	tmp->context = thr_context;
	tmp->waiting = NULL;
	return tmp;
}

LIST* newLIST(){
	LIST* tmp = (LIST*)malloc(sizeof(LIST));
	tmp->node = NULL;
	tmp->next = NULL;
	return tmp;
}

int addTCBinFIFO(TCB* atcb, FIFO* afifo){
	if(afifo == NULL || atcb == NULL){
		return -1;
	}
	LIST* aNewLIST = newLIST();
	aNewLIST->node = atcb;
	if(afifo->head==NULL){ //primeiro nodo
		afifo->head = aNewLIST;
		aNewLIST->next = afifo->head;
		afifo->tail = aNewLIST;	
	}
	else{
		afifo->tail->next = aNewLIST;
		aNewLIST->next = afifo->head;
		afifo->tail = aNewLIST;
	}
	return 0;
}

TCB* getFirst(FIFO* afifo){
	if(afifo->head!=NULL){
		return afifo->head->node;
	}
	else{
		return NULL;
	}
}

void remove_thread(TCB* thread, FIFO* afifo){
	LIST* tmp = afifo->head;
	if(afifo->head!=afifo->tail){
		while(tmp->next->node!=thread){ //TCB com TCB			
			tmp = tmp->next; //LIST
		}
		if(tmp->next == afifo->head){ //LIST x LIST			
			afifo->head = afifo->head->next; 
		}
		if(tmp->next == afifo->tail){
			afifo->tail = tmp;
		}
		tmp->next = tmp->next->next;
	}
	else{
		afifo->head=NULL;
		afifo->tail=NULL;
	}
}

/*
* Muda estado da thread para Running
*/
void toRunning(TCB* atcb){
	atcb->state = 2;
}
/*
* Muda estado da thread para Ready
*/
void toReady(TCB* atcb){
	atcb->state = 1;
}
/*
* Muda estado da thread para Blocked
*/
void toBlocked(TCB* atcb){
	atcb->state = 3;
}

void addinWaitList(TCB* TCBblocking,TCB* TCBblocked){
	LIST* tmp;	
	if(TCBblocking==NULL||TCBblocked==NULL){
		return;
	}
	if(TCBblocking->waiting==NULL){
		TCBblocking->waiting=newLIST();
		TCBblocking->waiting->node = TCBblocked;
	}
	else{
		tmp = TCBblocking->waiting;
		while(tmp->next!=NULL){
			tmp = tmp->next;
		}
		tmp->next = newLIST();
		tmp->next->node = TCBblocked;
	}
}

