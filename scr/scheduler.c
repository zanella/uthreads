#include <stdio.h>
#include "../include/scheduler.h"
#include "../include/dispatcher.h"

int counter_tid;
int counter_tcb;
FIFO* possible_tcb;
FIFO* blocked_tcb;
FIFO* all_tcb;
ucontext_t* endcontext;
/*
* Inicia escalonador
*/
int initScheduler(){
	endcontext = (ucontext_t*)malloc(sizeof(ucontext_t));	
	counter_tid = 0;
	counter_tcb = 0;
	possible_tcb = newFIFO();
	blocked_tcb = newFIFO();
	all_tcb = newFIFO();
	if(possible_tcb==NULL || blocked_tcb==NULL || all_tcb==NULL){
		return -1;
	}
	else{
		endcontext=make_context(endThread, NULL);
		if(endcontext==NULL){
			return -1;
		}
		return 0;
	}
}

/*
* Cria uma nova thread (contexto deve ser ajustado antes)
*/
int addthread(ucontext_t* thr_context){
	TCB* atcb = newTCB(thr_context, counter_tid);
	if(inc_counters()==1){		
		if(atcb==NULL){
			return -1;
		}
		if(addTCBinFIFO(atcb, all_tcb)==-1){		
			return -1;
		}
		if(addTCBinFIFO(atcb, possible_tcb)==-1){						
			return -1;
		}
		return atcb->thread_id;
	}
	else{
		free(atcb);
		return -1;
	}

}

/*
* Incrementa contadores de tid e tcb
*/
int inc_counters(){
	if((counter_tcb + 1) > MAXTHREADS){
		return 0;
	}
	else{
		counter_tid++;
		counter_tcb++;
		return 1;
	}
}

/*
* Finaliza as threads (onde uclink deve apontar)
*/
void endThread(){
	TCB* thread = getFirst(possible_tcb); //Thread que está executando	
	remove_thread(thread, possible_tcb);	
	remove_thread(thread, all_tcb);	
	unbloking(thread);
	free(thread);
	thread = NULL;
	counter_tcb--;
	next_thread();
}
/*
* Inicia a próxima thread a ser executada
*/
int next_thread(){
	TCB* newrun = getFirst(possible_tcb);
	if(newrun!=NULL){
		toRunning(newrun);
		load_context(newrun);
		return -1;
	}
	else{
		return 0; //SEM ERRO, apenas acabou a fila de thread
	}
}

ucontext_t* getEndContext(){
	return endcontext;
}

void inc_pointers(){
	possible_tcb->head = possible_tcb->head->next;
	possible_tcb->tail = possible_tcb->tail->next;
	toReady(possible_tcb->tail->node);
}

int yield_scheduler(){ 
	int ok;	
	TCB* running_thread = possible_tcb->head->node;
	ok = save_context(running_thread);
	return ok;
}

void printfifo(){ //para debug
	LIST* tmp;
	printf("hello");	
	if(blocked_tcb!=NULL){
	 tmp = blocked_tcb->head;
	printf("blocked:");		
	do{
		printf(" %d - ",tmp->node->thread_id);		
		tmp=tmp->next;
		
	}while(tmp!=blocked_tcb->head);
	
	printf("\n");
	}
	else{
		printf("ieie");
	}
	if(possible_tcb!=NULL){
	tmp = possible_tcb->head;
	printf("possible:");		
	do{
		printf(" %d - ",tmp->node->thread_id);		
		tmp=tmp->next;
		
	}while(tmp!=possible_tcb->head);
	printf("\n");
	}
}

void* getTCB(int thr){
	LIST* tmp = all_tcb->head;	
	do{
		if(tmp->node->thread_id==thr){
			return (void*)tmp->node;
		}
		tmp = tmp->next;
	}while(tmp!=all_tcb->head);
	return NULL;
}

void* getRunning(){	
	return possible_tcb->head->node;
}

int blocking(void* tcb_blocking, void* tcb_current){
	int returned = 0;
	TCB* TCBblocking = (TCB*) tcb_blocking;
	TCB* TCBblocked = (TCB*) tcb_current;
	addinWaitList(TCBblocking, TCBblocked);
	toBlocked(TCBblocked);
	save_context(TCBblocked);
	if(!returned){
		returned =1;
		remove_thread(TCBblocked, possible_tcb);
		addTCBinFIFO(TCBblocked, blocked_tcb);		
		next_thread();
	}
	return 0;
}

int unbloking(void* atcb_v){
	LIST* tmp;
	TCB* atcb = (TCB*)atcb_v;
	if(atcb->waiting!=NULL){
		tmp=atcb->waiting;				
		while(tmp!=NULL){
			toReady(tmp->node);
			remove_thread(tmp->node, blocked_tcb);
			addTCBinFIFO(tmp->node, possible_tcb);
			tmp = tmp->next;
		}
	}
	return 0;
}

void toBlockedList(void* tcb){
	TCB* atcb = (TCB*)tcb;
	remove_thread(atcb, possible_tcb);
	addTCBinFIFO(atcb, blocked_tcb);
	save_context(atcb);
}

void toReadyList(void* afifo){
	if(afifo!=NULL){
		FIFO* lockfifo = (FIFO*) afifo;
		TCB* atcb = getFirst(lockfifo);
		if(atcb!=NULL){
			toReady(atcb);			
			addTCBinFIFO(atcb, possible_tcb);
			remove_thread(atcb, blocked_tcb);
			remove_thread(atcb, lockfifo);	
		}
	}
}
