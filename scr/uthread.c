
#include "../include/uthread.h"
#include "../include/thread_context.h"
#include "../include/scheduler.h"

int libuthread_init(){	
	ucontext_t* first_context = (ucontext_t*) malloc(sizeof(ucontext_t));
	int hasInitialized = 0;	
	int error; 
	initScheduler(); //inicialização do escalonador
	error=getcontext(first_context);
	if(!hasInitialized){	
		hasInitialized = 1;		
		if(error==0){
			if(addthread(first_context)!=-1){ //cria primeira thread
				next_thread();
			}
		}
		else{
			return error;
		}
	}
	return 0;
}

int uthread_create(void*(*start_routine)(void*), void* arg){
	ucontext_t* endcontext = getEndContext();
	ucontext_t* newcontextThread = make_context_args(start_routine, endcontext, arg); //captura contexto da nova thread
	if (newcontextThread==NULL){
		return -1;
	}
	return addthread(newcontextThread);
}

int uthread_yield(){
	int flag_y = 0;
	int ok;	
	ok = yield_scheduler(); //salva contexto da thread atual para fazer o yield
	if(flag_y==0){
		flag_y=1;
		//translada ponteiros, fazendo a thread atual ir para o final da fila, e a proxima thread, no inicio da fila.
		inc_pointers(); 
		if(next_thread()==-1){
			return -1;
		}
	}
	return ok;
}

int uthread_join(int thr){
	TCB* tcb_blocking = (TCB*) getTCB(thr);
	if(tcb_blocking!=NULL){
		TCB* tcb_current = (TCB*) getRunning();
		if(tcb_current!=NULL){
			return blocking(tcb_blocking, tcb_current); //bloqueia a thread tcb_current até a tcb_current terminar
		}
	}
	return 0;
}

void uthread_mutex_init (umutex_t* lock){	
	lock->flag = 0;
	lock->tcb_list = newFIFO();
}

int uthread_lock(umutex_t* lock){
	int ok = 0;	
	TCB* tcb_running = (TCB*) getRunning();	
	if(lock==NULL){		
		return -1;
	}
	if(lock->flag==0){	
		lock->flag=1;
		return 0;
	}
	else{ 
		//bloqueia a thread atual se zona crítica já estiver sendo ocupada
		FIFO* afifo = lock->tcb_list;
		addTCBinFIFO(tcb_running, afifo);
		toBlocked(tcb_running);
		toBlockedList(tcb_running);
		if(ok==0){
			ok=1;
			next_thread();
			return -1;
		}
		return 0;
	}
}

int uthread_unlock(umutex_t* lock){
	FIFO* afifo = NULL;
	lock->flag = 0;
	if(lock->tcb_list!=NULL){
		if(lock->tcb_list->head!=NULL){			
			afifo = lock->tcb_list;	
			toReadyList(afifo); //desbloqueia thread
		}
	}
	lock->flag=1;
	return 0;
}
