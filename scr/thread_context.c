#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include "../include/thread_context.h"
#define STACKSIZE 10485760

ucontext_t* make_context(void (*start_routine) (void), ucontext_t* endlink){
	ucontext_t* anewcontext = (ucontext_t*)malloc(sizeof(ucontext_t));
	if(anewcontext!=NULL){
		getcontext(anewcontext);
		anewcontext->uc_stack.ss_sp = mmap(NULL,STACKSIZE,PROT_WRITE|PROT_READ,MAP_PRIVATE|MAP_GROWSDOWN|MAP_ANONYMOUS,-1,0);
		anewcontext->uc_stack.ss_size = STACKSIZE;
		anewcontext->uc_link = endlink;

		makecontext(anewcontext, start_routine, 0);

		return anewcontext;
	}
	else{
		return NULL;
	}
}

ucontext_t* make_context_args(void* (*start_routine) (void*), ucontext_t* endlink, void* arg){
	ucontext_t* anewcontext = (ucontext_t*)malloc(sizeof(ucontext_t));
	if(anewcontext!=NULL){
		getcontext(anewcontext);
		anewcontext->uc_stack.ss_sp = mmap(NULL,STACKSIZE,PROT_WRITE|PROT_READ,MAP_PRIVATE|MAP_GROWSDOWN|MAP_ANONYMOUS,-1,0);
		anewcontext->uc_stack.ss_size = STACKSIZE;
		anewcontext->uc_link = endlink;

		makecontext(anewcontext, (void (*)(void))start_routine, 1, arg);

		return anewcontext;
	}
	else{
		return NULL;
	}
}
